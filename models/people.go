package models

type People struct {
	ID       uint   `json:"id" gorm:"primary_key"`
	Name     string `json:"name"`
	LastName string `json:"lastname"`
}
