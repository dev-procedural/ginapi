package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func MIDDLE() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("X-STUPI", "OK")
		c.Next()
	}

}

func checkuser() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("check user ")
	}
}
