module micro

go 1.15

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/davecgh/go-spew v1.1.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
)
