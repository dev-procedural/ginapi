package main

import (
	"log"
	"micro/controller"
	"micro/middleware"
	"micro/models"
   "fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	models.ConnectDatabase()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	errInit := authMiddleware.MiddlewareInit()
	if errInit != nil {
		log.Fatal("authMiddleware.MiddlewareInit() Error:" + errInit.Error())
	}

	r.GET("/ping", func(c *gin.Context) {
		//take action
		c.JSON(200, gin.H{"msg": "Pong"})
	})

	r.POST("/hello", middleware.MIDDLE(), controller.Hello)
	r.POST("/getall", middleware.MIDDLE(), controller.GetAll)
	r.POST("/create", middleware.MIDDLE(), controller.CreatePerson)

	r.POST("/getuser", controller.GetUser)
	r.POST("/getusers", controller.GetAllUsers)

   fmt.Println("This is a test")
   fmt.Println("This is a second test")
	r.Run()
}
