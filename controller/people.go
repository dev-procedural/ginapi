package controller

import (
	"micro/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

//_________ GET ALL PEOPLE
func GetAll(c *gin.Context) {
	var p []models.People
	models.DB.Find(&p)

	c.JSON(http.StatusOK, gin.H{"msg": p})

}

//_________ CREATE PEOPLE
type RequestData struct {
	Name     string `form:name json:name binding:"required"`
	LastName string `form:lastname json:lastname binding:"required"`
}

func CreatePerson(c *gin.Context) {
	var req RequestData
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	people := models.People{Name: req.Name, LastName: req.LastName}
	models.DB.Create(&people)

	c.JSON(http.StatusOK, gin.H{"data": people})
}
