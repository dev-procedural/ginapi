package controller

import (
	"fmt"
	"micro/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

//_________ GET ALL PEOPLE
func GetAllUsers(c *gin.Context) {
	var p []models.People
	models.DB.Find(&p)

	c.JSON(http.StatusOK, gin.H{"msg": p})

}

//_________ GET USER
type RequestDataUser struct {
	Name     string `form:name json:name binding:"required"`
	LastName string `form:lastname json:lastname`
}

// type Msg struct {
// 	Data
// }

func GetUser(c *gin.Context) {
	var req RequestDataUser
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var p models.People
	models.DB.Where("name = ?", req.Name).First(&p)
	if p.ID == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "No User Found"})
		return
	}

	fmt.Println(fmt.Sprintf("%T", p))
	// spew.Dump(p.ID)
	c.JSON(http.StatusOK, gin.H{"msg": &p})

}

// func GetUsers(c *gin.Context) {
// 	var req RequestDataUser
// 	if err := c.ShouldBindJSON(&req); err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": "Bad payload"})
// 		return
// 	}

// 	// TEST ERROR
// 	var all []models.People
// 	models.DB.Where("name <> ?", "JJ").Find(&all)
// 	if len(all) == 0 {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": "No User Found"})
// 		return
// 	}

// 	fmt.Println(fmt.Sprintf("%T", all))
// 	// spew.Dump(all.ID)
// 	c.JSON(http.StatusOK, gin.H{"msg": &all})

// }
