package controller

import (
	"github.com/gin-gonic/gin"
)

type Login struct {
	User     string `form:"user" json:"user" xml:"user"  binding:"required"`
	Password string `form:"password" json:"password" xml:"password" binding:"required"`
}

func Hello(c *gin.Context) {
	var data Login
	c.Bind(&data)
	c.JSON(200, gin.H{
		"mgs":    data.User,
		"status": "Hello",
	})
}
